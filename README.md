//TO-DO: Project description xD


## Progress & Not Translated (yet) lists
- [X] [UI.tsv](languageData/English/UI.tsv) : 99%
    - [UI_20150923_001803](languageData/English/UI.tsv#L1803)
    - [UI_20160518_002143](languageData/English/UI.tsv#L2143)
    - [UI_20160518_002144](languageData/English/UI.tsv#L2144)
    - [UI_20160518_002146](languageData/English/UI.tsv#L2146)
    - [UI_20160519_002147](languageData/English/UI.tsv#L2147)
    - [UI_20160520_002148](languageData/English/UI.tsv#L2148)
    - [UI_20161212_002394](languageData/English/UI.tsv#L2394)
- [ ] [SKILL.tsv](languageData/English/SKILL.tsv) : 0%
- [ ] [ITEM.tsv](languageData/English/ITEM.tsv) : 0%
- [ ] [QUEST.tsv](languageData/English/QUEST.tsv) : 0%
- [X] [QUEST_JOBSTEP.tsv](languageData/English/QUEST_JOBSTEP.tsv) : 99%
    - [INTL_20160511_000002](languageData/English/INTL.tsv#L2)
    - [INTL_20160511_000003](languageData/English/INTL.tsv#L3)
    - [INTL_20160511_000004](languageData/English/INTL.tsv#L4)
    - [INTL_20160511_000005](languageData/English/INTL.tsv#L5)
    - [INTL_20160511_000006](languageData/English/INTL.tsv#L6)
    - [INTL_20160511_000009](languageData/English/INTL.tsv#L9)
    - [INTL_20160511_000016](languageData/English/INTL.tsv#L16)
    - [INTL_20160511_000017](languageData/English/INTL.tsv#L17)
    - [INTL_20160511_000018](languageData/English/INTL.tsv#L18)
    - [INTL_20160511_000027](languageData/English/INTL.tsv#L27)
    - [INTL_20160511_000028](languageData/English/INTL.tsv#L28)
    - [INTL_20160517_000030](languageData/English/INTL.tsv#L30)
    - [INTL_20160517_000031](languageData/English/INTL.tsv#L31)
    - [INTL_20160517_000034](languageData/English/INTL.tsv#L34)
    - [INTL_20160517_000035](languageData/English/INTL.tsv#L35)
    - [INTL_20160616_000036](languageData/English/INTL.tsv#L36)
    - [INTL_20160616_000037](languageData/English/INTL.tsv#L37)
    - [INTL_20160616_000038](languageData/English/INTL.tsv#L38)
    - [INTL_20160616_000039](languageData/English/INTL.tsv#L39)
- [ ] [QUEST_LV_0100.tsv](languageData/English/QUEST_LV_0100.tsv) : 0%
- [ ] [QUEST_LV_0200.tsv](languageData/English/QUEST_LV_0200.tsv) : 0%
- [ ] [QUEST_LV_0300.tsv](languageData/English/QUEST_LV_0300.tsv) : 0%
- [ ] [QUEST_LV_0400.tsv](languageData/English/QUEST_LV_0400.tsv) : 0%
- [ ] [QUEST_UNUSED.tsv](languageData/English/QUEST_UNUSED.tsv) : 0%
- [ ] [ETC.tsv](languageData/English/ETC.tsv) : 0%
- [X] [INTL.tsv](languageData/English/INTL.tsv) : 61%
    - [QUEST_JOBSTEP_20150811_003131](languageData/English/QUEST_JOBSTEP.tsv#L3131)
    - [QUEST_JOBSTEP_20150811_003132](languageData/English/QUEST_JOBSTEP.tsv#L3132)
    - [QUEST_JOBSTEP_20150811_003133](languageData/English/QUEST_JOBSTEP.tsv#L3133)
    - [QUEST_JOBSTEP_20150811_003134](languageData/English/QUEST_JOBSTEP.tsv#L3134)
    - [QUEST_JOBSTEP_20150811_003135](languageData/English/QUEST_JOBSTEP.tsv#L3135)
    - [QUEST_JOBSTEP_20150811_003136](languageData/English/QUEST_JOBSTEP.tsv#L3136)
    - [QUEST_JOBSTEP_20150811_003137](languageData/English/QUEST_JOBSTEP.tsv#L3137)
    - [QUEST_JOBSTEP_20150811_003138](languageData/English/QUEST_JOBSTEP.tsv#L3138)
    - [QUEST_JOBSTEP_20150811_003139](languageData/English/QUEST_JOBSTEP.tsv#L3139)
    - [QUEST_JOBSTEP_20150811_003140](languageData/English/QUEST_JOBSTEP.tsv#L3140)


## Note:
1. [QUEST_JOBSTEP.tsv](languageData/English/QUEST_JOBSTEP.tsv) :
    - *QUEST_JOBSTEP_20150811_003131* ~ *QUEST_JOBSTEP_20150811_003140* on ToS INA doesn't exist on iToS
    - *QUEST_JOBSTEP_20150908_003131* ~ *QUEST_JOBSTEP_20150908_003138* on iToS doesn't exist on ToS INA (yet?)

